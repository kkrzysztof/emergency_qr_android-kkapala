package com.thegoodhealthnetwork.healthei.model;

/**
 * Created by kamil on 19.01.2016.
 */
public class ContactElement {

    public ContactElement(int type, String text) {
        this.type = type;
        this.text = text;
    }

    public static final int PHONE = 1;
    public static final int SMS = 2;
    public static final int EMAIL = 3;
    public static final int LINK = 4;

    private int type;

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
