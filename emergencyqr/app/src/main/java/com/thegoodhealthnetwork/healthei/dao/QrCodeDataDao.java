package com.thegoodhealthnetwork.healthei.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import android.util.Log;
import com.thegoodhealthnetwork.healthei.global.Global;
import com.thegoodhealthnetwork.healthei.model.QRCodeDataModel;

public class QrCodeDataDao {

	private static final String TAG = "QrCodeDataDao";
	
	private File qrCodeDataFile;
	
	public QrCodeDataDao() {
		String qrCodeDataFolder = "qrcodedata/";
		
		File qrCodeDataFolderFile = new File(Global.getIntegrnalApplicationFilePaht()+ qrCodeDataFolder);
		
		if(!qrCodeDataFolderFile.exists()) {
			qrCodeDataFolderFile.mkdir();
		}
		
		qrCodeDataFile = new File(qrCodeDataFolderFile,"qrcodedata.dat");
	}
	
	
	public void saveQrCodeData(QRCodeDataModel qRCodeDataModel) throws Exception {
		try {
			FileOutputStream fos = new FileOutputStream(qrCodeDataFile);
			
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(qRCodeDataModel);
			oos.close();
		}
		catch(IOException e) {
			Log.e(TAG, "Can't save qrcode data to file "+qrCodeDataFile.getAbsolutePath());
			throw new Exception("Can't save data");
		}
	}
	
	
	public QRCodeDataModel loadQrCodeData() throws Exception {
		QRCodeDataModel qrCodeDataModel = new QRCodeDataModel();
		
		if(!qrCodeDataFile.exists()) {
			return qrCodeDataModel;
		}
		
		try {
			FileInputStream fis = new FileInputStream(qrCodeDataFile);
			
			ObjectInputStream ois = new ObjectInputStream(fis);
			qrCodeDataModel = (QRCodeDataModel)ois.readObject();
			ois.close();
			
			return qrCodeDataModel;
		}
		catch(IOException e) {
			Log.e(TAG, "Can't read qrcode data from file "+qrCodeDataFile.getAbsolutePath());
			throw new Exception("Can't read qr code data");
		}
	}

}
