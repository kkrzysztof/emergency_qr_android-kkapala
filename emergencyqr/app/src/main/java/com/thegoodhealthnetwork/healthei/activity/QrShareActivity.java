package com.thegoodhealthnetwork.healthei.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.thegoodhealthnetwork.healthei.R;
import com.thegoodhealthnetwork.healthei.dialogs.ErrorDialog;
import com.thegoodhealthnetwork.healthei.dialogs.ShareActionsDialog;
import com.thegoodhealthnetwork.healthei.dialogs.ShareActionsDialog.ShareActionsDialogListener;
import com.thegoodhealthnetwork.healthei.generator.QrCodeImageGenerator;
import com.thegoodhealthnetwork.healthei.satatistic.StatisticManager;

public class QrShareActivity extends Activity {
	private ImageView qrCodeImage;
	private ShareActionsDialog shareActionsDialog;
	private Bitmap qrCodeImageBitmap;
	private CallbackManager callbackManager;
	private ShareDialog shareDialog;

	public static final int SHARE_ACTIVITY_CANCELED =-400;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_qr_share);

		shareDialog = new ShareDialog(this);

		qrCodeImage = (ImageView) findViewById(R.id.qrCodeImage);

		Button shareButton = (Button) findViewById(R.id.shareActionsBtn);
		shareButton.setOnClickListener(new ActionsButtonCLickListener());

		callbackManager = CallbackManager.Factory.create();
		shareDialog.registerCallback(callbackManager, new FacebookCallbackImpl());

		generateQrCode();
	}

	private void generateQrCode() {
		QrCodeImageGenerator qrCodeImageGenerator = new QrCodeImageGenerator(
				this);
		qrCodeImageBitmap = qrCodeImageGenerator.generateCode(this.getString(R.string.prommotionQrCodeValue),
				false);

		qrCodeImage.setImageBitmap(qrCodeImageBitmap);
	}

	private class ActionsButtonCLickListener implements
			android.view.View.OnClickListener {

		@Override
		public void onClick(View arg0) {
			shareActionsDialog = new ShareActionsDialog(
					QrShareActivity.this);
			shareActionsDialog
					.setShareActionsDialogListener(new ShareActionsDialogListenerImpl());
			shareActionsDialog.show();
		}

	}

	private class ShareActionsDialogListenerImpl implements
			ShareActionsDialogListener {

		@Override
		public void onSendClick() {
				StatisticManager.increaseCodeShared(new Date(System.currentTimeMillis()));
				
				ArrayList<Uri> imagesUris = new ArrayList<>();
				String pathToImage = saveQrCodeImageToFile();
				File imageFile = new File(pathToImage);
				Uri uri = Uri.parse("file://" + imageFile.getAbsolutePath());
				imagesUris.add(uri);
				
		        final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND_MULTIPLE);
		        emailIntent.putExtra(Intent.EXTRA_STREAM,imagesUris);
		        emailIntent.putExtra(Intent.EXTRA_TEXT, QrShareActivity.this.getString(R.string.promotionEmailBody));
		        emailIntent.putExtra(Intent.EXTRA_SUBJECT, QrShareActivity.this.getString(R.string.promotionEmailSubject));
		        emailIntent.setType("image/*");
		        startActivity(Intent.createChooser(emailIntent,"Send email by ..."));
		}

		@Override
		public void onPrintClick() {
			// TODO Auto-generated method stub

		}

		@Override
		public void onPostFacebooClick() {
			shareActionsDialog.cancel();

			ShareLinkContent content= new ShareLinkContent.Builder()
					.setContentTitle(QrShareActivity.this.getString(R.string.app_name))
					.setContentDescription(QrShareActivity.this.getString(R.string.promotionFacebookValue))
					.setImageUrl(Uri.parse(QrShareActivity.this.getString(R.string.facebookPromotionImageLink)))
					.setContentUrl(Uri.parse(QrShareActivity.this.getString(R.string.prommotionQrCodeValue))).build();

			shareDialog.show(content,ShareDialog.Mode.NATIVE);
		}

		@Override
		public void onPostTwitterClick() {

			String pathToImage = saveQrCodeImageToFile();

			Uri myImageUri = Uri.fromFile(new File(pathToImage));

			TweetComposer.Builder builder = new TweetComposer.Builder(QrShareActivity.this)
					.text(QrShareActivity.this.getString(R.string.promotionTwitterValue))
					.image(myImageUri);
			builder.show();
		}

		@Override
		public void onSendWhatsAppClick() {
			StatisticManager.increaseCodeShared(new Date(System.currentTimeMillis()));
			
			shareActionsDialog.cancel();
			boolean whatsappInstalled = false;

			String pathToImage = saveQrCodeImageToFile();

			PackageManager pm = getPackageManager();
			Intent sendIntent = new Intent(Intent.ACTION_SEND);
			sendIntent.setType("text/plain");

			List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);

			String whatsAppMessage = QrShareActivity.this.getString(R.string.promotionWhatsAppValue);


			for (int i = 0; i < resInfo.size(); i++) {
				// Extract the label, append it, and repackage it in a
				// LabeledIntent
				ResolveInfo ri = resInfo.get(i);
				String packageName = ri.activityInfo.packageName;
				if (packageName.contains("com.whatsapp")) {
					whatsappInstalled = true;

					Intent waIntent = new Intent(Intent.ACTION_SEND);
					waIntent.setType("image/*");
					
					waIntent.setPackage("com.whatsapp");

					waIntent.putExtra(Intent.EXTRA_TEXT, whatsAppMessage);
					waIntent.putExtra(Intent.EXTRA_STREAM,
							Uri.fromFile(new File(pathToImage)));
					waIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(Intent.createChooser(waIntent, "Share with"));
				}
			}
			
			if(!whatsappInstalled) {
				ErrorDialog errorDialog = new ErrorDialog(QrShareActivity.this, "Whatsapp is not installed");
				errorDialog.show();
				
			}
		}

		@Override
		public void onSendMessageWhatsAppClick() {
			shareActionsDialog.cancel();
			boolean whatsappInstalled = false;

			String whatsAppMessage = QrShareActivity.this.getString(R.string.promotionWhatsAppValue);

			PackageManager pm = getPackageManager();
			Intent sendIntent = new Intent(Intent.ACTION_SEND);
			sendIntent.setType("text/plain");

			List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
			for (int i = 0; i < resInfo.size(); i++) {
				// Extract the label, append it, and repackage it in a
				// LabeledIntent
				ResolveInfo ri = resInfo.get(i);
				String packageName = ri.activityInfo.packageName;
				if (packageName.contains("com.whatsapp")) {
					whatsappInstalled = true;

					Intent waIntent = new Intent(Intent.ACTION_SEND);
					waIntent.setType("text/*");
					
					waIntent.setPackage("com.whatsapp");

					//waIntent.putExtra(Intent.EXTRA_TEXT, text);
					waIntent.putExtra(Intent.EXTRA_TEXT,whatsAppMessage);
					waIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(Intent.createChooser(waIntent, "Share with"));
				}
			}
			
			if(!whatsappInstalled) {
				ErrorDialog errorDialog = new ErrorDialog(QrShareActivity.this, "Whatsapp is not installed");
				errorDialog.show();
				
			}
			
		}
	}

	@Override
	protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		callbackManager.onActivityResult(requestCode, resultCode, data);
	}

	private String saveQrCodeImageToFile() {
		String pathToFolder = Environment.getExternalStoragePublicDirectory(getResources().getString(R.string.app_folder_name)).getAbsolutePath();
		
		File folderFile = new File(pathToFolder);
		
		if(!folderFile.exists()) {
			folderFile.mkdir();
			Log.d("QrShareActivity", "Folder created");
		}
		
		File qrcodepromotion = new File(folderFile, "qrcodepromotion_"+System.currentTimeMillis()+".jpg");

		FileOutputStream fos = null;
		try {

			fos = new FileOutputStream(qrcodepromotion);
			qrCodeImageBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return qrcodepromotion.getAbsolutePath();
	}
	
	private boolean isNetworkAvailable() {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	
	private void showNettworkError() {
		ErrorDialog errorDialog = new ErrorDialog(this,getString(R.string.networkConnectionError));
		errorDialog.show();
	}

	private class FacebookCallbackImpl implements FacebookCallback<Sharer.Result> {

		@Override
		public void onSuccess(Sharer.Result result) {
			Toast.makeText(QrShareActivity.this,"Facebook share done",Toast.LENGTH_LONG);
		}

		@Override
		public void onCancel() {
			Toast.makeText(QrShareActivity.this,"Facebook share canceled",Toast.LENGTH_LONG);
		}

		@Override
		public void onError(FacebookException e) {
			ErrorDialog errorDialog = new ErrorDialog(QrShareActivity.this,e.getMessage()+" Please instal newest version of facebook android app.");
			errorDialog.show();
		}
	}

	@Override
	public void onBackPressed() {
		setResult(SHARE_ACTIVITY_CANCELED);
		finish();
	}
}
