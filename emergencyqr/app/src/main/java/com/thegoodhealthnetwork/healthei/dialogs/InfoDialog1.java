package com.thegoodhealthnetwork.healthei.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.thegoodhealthnetwork.healthei.R;

public class InfoDialog1 extends Dialog {
	private TextView infoViewMessage;
	private TextView titletView;
	private TextView buttonTextView;

	public InfoDialog1(Context context) {
		super(context);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.info_dialog1);

		setCanceledOnTouchOutside(false);
		setCancelable(false);

		getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

		FrameLayout nextButton = (FrameLayout)findViewById(R.id.nextButton);
		nextButton.setOnClickListener(new NextButtonCLickListener());

		infoViewMessage = (TextView)findViewById(R.id.infoViewMessage);
		titletView = (TextView)findViewById(R.id.titletView);
		buttonTextView = (TextView)findViewById(R.id.buttonTextView);

	}

	public void setInfoTitle(String text) {
		titletView.setText(text);
	}

	public void setInfoText(String text) {
 		infoViewMessage.setText(text);
	}

	public void setButtonText(String text) {
		buttonTextView.setText(text);
	}

	public interface InfoDialog1Listener {
		public void nextClick();
	}

	private class NextButtonCLickListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {

			if(infoDialog1Listener!=null) {
				infoDialog1Listener.nextClick();
			}

			InfoDialog1.this.cancel();
		}
	}


	private InfoDialog1Listener infoDialog1Listener;

	public InfoDialog1Listener getInfoDialog1Listener() {
		return infoDialog1Listener;
	}

	public void setInfoDialog1Listener(InfoDialog1Listener infoDialog1Listener) {
		this.infoDialog1Listener = infoDialog1Listener;
	}
}
