package com.thegoodhealthnetwork.healthei.generator;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.google.zxing.WriterException;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.google.zxing.qrcode.encoder.ByteMatrix;
import com.google.zxing.qrcode.encoder.Encoder;
import com.google.zxing.qrcode.encoder.QRCode;

import com.thegoodhealthnetwork.healthei.R;

public class QrCodeImageGenerator {
	private Context context;
	private int screenWidth;
	private int screenHeight;
	
	public QrCodeImageGenerator(Context context) {
		this.context = context;
		
		DisplayMetrics displayMetrics = new DisplayMetrics();
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE); 
		wm.getDefaultDisplay().getMetrics(displayMetrics);
		screenWidth = displayMetrics.widthPixels;
		screenHeight = displayMetrics.heightPixels;
	}
	
	
	public Bitmap generateCode(String text, boolean withBackground) {
		QRCode qrc = null;
		
		try {
			qrc = Encoder.encode(text, ErrorCorrectionLevel.Q);
		} catch (WriterException e) {
			e.printStackTrace();
		}

		ByteMatrix bm = qrc.getMatrix();

		Bitmap qrCodeBitmap = createBitmap(bm.getWidth(),transform(bm.getArray()));
		
		if(!withBackground) {
			return addIconBackground(qrCodeBitmap);
		} 
		else {
			return createBackgroundBitmap(qrCodeBitmap);
		}
	}


	public Bitmap generateCodeForEmail(String text) {
		QRCode qrc = null;

		try {
			qrc = Encoder.encode(text, ErrorCorrectionLevel.Q);
		} catch (WriterException e) {
			e.printStackTrace();
		}

		ByteMatrix bm = qrc.getMatrix();

		Bitmap qrCodeBitmap = createBitmap(bm.getWidth(),transform(bm.getArray()));

		return addIconBackground2(qrCodeBitmap);
	}
	
	private byte[][] transform(byte[][] data) {
		
		byte[][] transformed = new byte[data.length][data.length];
		
		for(int i=0;i<data.length;i++) {
			for(int j=0;j<transformed.length;j++) {
				transformed[i][j] = data[j][i];
			}
		}
		
		return transformed;
	}

	private Bitmap createBitmap(int size, byte[][] data) {
		Bitmap b = Bitmap.createBitmap(size, size, Config.ARGB_8888);

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {

				if (data[i][j] == 1) {
					b.setPixel(i, j, Color.rgb( 0, 0, 0));
				} else {
					b.setPixel(i, j,Color.TRANSPARENT);
				}
	
			}
		}
		
		return Bitmap.createScaledBitmap(b, 500, 500, false);
	}
	
	private Bitmap addIconBackground(Bitmap qrCode) {
		Bitmap.Config conf = Bitmap.Config.ARGB_8888;
		
		int scaledBgIconCodeBitmapSize = (int) (screenWidth*0.7);
		
		Bitmap bmp = Bitmap.createBitmap(scaledBgIconCodeBitmapSize, scaledBgIconCodeBitmapSize, conf);
		
		Canvas canvas = new Canvas(bmp);

		Bitmap iconBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.qrcode_icon_bg);
		Bitmap scaledBgIconQrCode = Bitmap.createScaledBitmap(iconBitmap, scaledBgIconCodeBitmapSize, scaledBgIconCodeBitmapSize, false);
		canvas.drawBitmap(scaledBgIconQrCode, 0, 0, null);
		
		int scaledQrCodeBitmapSize = (int) (screenWidth*0.6);
		int marginSiez = (int) ((scaledBgIconCodeBitmapSize-scaledQrCodeBitmapSize)/2);

		Bitmap scaledQrCode = Bitmap.createScaledBitmap(qrCode, scaledQrCodeBitmapSize, scaledQrCodeBitmapSize, false);
	   
	    canvas.drawBitmap(scaledQrCode, marginSiez, marginSiez, null);
		
		return bmp;
	}

	private Bitmap addIconBackground2(Bitmap qrCode) {
		Bitmap.Config conf = Bitmap.Config.ARGB_8888;

		int scaledBgIconCodeBitmapSize = (int) (screenWidth*0.86);

		Bitmap bmp = Bitmap.createBitmap(scaledBgIconCodeBitmapSize, scaledBgIconCodeBitmapSize, conf);

		for (int i = 0; i < scaledBgIconCodeBitmapSize; i++) {
			for (int j = 0; j < scaledBgIconCodeBitmapSize; j++) {
				bmp.setPixel(i, j,Color.WHITE);
			}
		}


		Canvas canvas = new Canvas(bmp);

		int scaledBgIconCodeBitmapSize2 = (int) (screenWidth*0.7);
		int marginSiez0 = (int) ((scaledBgIconCodeBitmapSize-scaledBgIconCodeBitmapSize2)/2);
		Bitmap iconBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.qrcode_icon_bg);
		Bitmap scaledBgIconQrCode = Bitmap.createScaledBitmap(iconBitmap, scaledBgIconCodeBitmapSize2, scaledBgIconCodeBitmapSize2, false);
		canvas.drawBitmap(scaledBgIconQrCode, marginSiez0, 50, null);


		float scale = context.getResources().getDisplayMetrics().density;

		int fontSize = getFontSize(scaledBgIconQrCode.getHeight(), scale);

		int yPosition = (screenHeight-scaledBgIconCodeBitmapSize)/2;
		String text1 = context.getString(R.string.qrCodeImageText3a);
		String text2 = context.getString(R.string.qrCodeImageText3b);

		Paint paint = new Paint();
		paint.setTextSize((int) (fontSize * scale));
		paint.setColor(Color.rgb(31, 1715, 84));

		Rect bounds = new Rect();
		paint.getTextBounds(text1 + text2, 0, text1.length() + text2.length(), bounds);
		int x = ((bmp.getWidth() - bounds.width())/2)-bounds.left;

		canvas.drawText(text1, x, (float) (scaledBgIconCodeBitmapSize2 + bounds.height()+75), paint);


		paint = new Paint();
		paint.setTextSize((int) (fontSize * scale));
		paint.setColor(Color.rgb(31, 1715, 84));
		paint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

		bounds = new Rect();
		paint.getTextBounds(text1, 0, text1.length(), bounds);
		int x2 = bounds.width();

		canvas.drawText(text2,x+x2, (float) (scaledBgIconCodeBitmapSize2 + bounds.height()+75), paint);


		paint = new Paint();
		paint.setTextSize((int) (fontSize * scale));
		paint.setColor(Color.GREEN);
		paint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));


		int scaledQrCodeBitmapSize = (int) (screenWidth*0.7);
		int marginSiez = (int) ((scaledBgIconCodeBitmapSize-scaledQrCodeBitmapSize)/2);

		Bitmap scaledQrCode = Bitmap.createScaledBitmap(qrCode, scaledQrCodeBitmapSize, scaledQrCodeBitmapSize, false);

		canvas.drawBitmap(scaledQrCode, marginSiez, 50, null);

		return bmp;
	}




	private Bitmap createBackgroundBitmap(Bitmap qrCode) {
		Bitmap.Config conf = Bitmap.Config.ARGB_8888;
		
		
		
		screenHeight=screenHeight-dpToPx(40);
		
		Bitmap bmp = Bitmap.createBitmap(screenWidth, screenHeight, conf); 
		
		Canvas canvas = new Canvas(bmp);
		
		canvas.drawARGB(255, 175, 213, 109);
		
		//int qrBgIconCodeMarginSize = (int) (screenWidth*0.15);
		int scaledBgIconCodeBitmapSize = (int) (screenWidth*0.7);
		
		int yPosition = (screenHeight-scaledBgIconCodeBitmapSize)/2;
		
		
		
		Bitmap iconBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.qrcode_icon_bg);
		Bitmap scaledBgIconQrCode = Bitmap.createScaledBitmap(iconBitmap, scaledBgIconCodeBitmapSize, scaledBgIconCodeBitmapSize, false);
		
		int x1 = (bmp.getWidth() - scaledBgIconQrCode.getWidth())/2;
		canvas.drawBitmap(scaledBgIconQrCode, x1, yPosition, null);
		
		float scale = context.getResources().getDisplayMetrics().density;
		
		int fontSize = getFontSize(scaledBgIconQrCode.getHeight(), scale);
		
		String text1 = context.getString(R.string.qrCodeImageText1);
		String text2 = context.getString(R.string.qrCodeImageText2);
		
		Paint paint = new Paint();
		paint.setTextSize((int) (fontSize * scale));
		paint.setColor(Color.RED);
		
		Rect bounds = new Rect();
		paint.getTextBounds(text1, 0, text1.length(), bounds);
		int x = ((bmp.getWidth() - bounds.width())/2)-bounds.left;
		
	
		int scaledQrCodeBitmapSize = (int) (screenWidth*0.66);
		int yPosition2 = (screenHeight-scaledQrCodeBitmapSize)/2;
		
		canvas.drawText(text1, x, (float)(yPosition-(bounds.height()*0.2)), paint);
	
		Bitmap scaledQrCode = Bitmap.createScaledBitmap(qrCode, scaledQrCodeBitmapSize, scaledQrCodeBitmapSize, false);
		
		int qrCodeMarginSize = x1+((scaledBgIconQrCode.getHeight()-scaledQrCode.getHeight())/2);
	
	    canvas.drawBitmap(scaledQrCode, qrCodeMarginSize, yPosition2, null);
	    
	    Rect bounds2 = new Rect();
	    paint.setTextSize((int) (fontSize * scale));
		paint.getTextBounds(text2, 0, text2.length(), bounds2);
		
		x = ((bmp.getWidth() - bounds2.width())/2)-bounds2.left;
	
		canvas.drawText(text2, x,(int)(yPosition2+scaledBgIconCodeBitmapSize+(0.1*scaledBgIconCodeBitmapSize)), paint);
		
	    return bmp;
	}
	
	private int dpToPx(int dp) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dp * scale + 0.5f);
	}
	
	private int getFontSize(int imageHeight,float scale) {
		
		String text = "Emergency Scan";
		            
		int maxTextHeight = imageHeight/8;
		
		Paint paint = new Paint();
		
		int fontSize = 0;
		
		paint.setColor(Color.RED);
		Rect bounds = new Rect();
		
		while(bounds.height()<=maxTextHeight && bounds.width()<imageHeight) {
			fontSize++;
			paint.setTextSize((int) (fontSize * scale));
			paint.getTextBounds(text, 0, text.length(), bounds);
			
		}
		return fontSize-3;
	}
}
