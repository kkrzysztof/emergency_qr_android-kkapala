package com.thegoodhealthnetwork.healthei.activity;

import com.thegoodhealthnetwork.healthei.R;
import com.thegoodhealthnetwork.healthei.generator.QrCodeImageGenerator;
import com.thegoodhealthnetwork.healthei.model.QRCodeDataModel;
import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;

public class QrPreviewActivity extends Activity {

	public static final int PREVIEW_ACTIVITY_CANCELED =-200;
	private ImageView qrCodeImage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		QRCodeDataModel qrcodedata = (QRCodeDataModel)getIntent().getSerializableExtra("qrcodedata");
		
		setContentView(R.layout.activity_qr_preview);

		qrCodeImage = (ImageView)findViewById(R.id.qrCodeImage);
		
		generateQrCode(qrcodedata);
	}
	
	private void generateQrCode(QRCodeDataModel qRCodeDataModel) {
		QrCodeImageGenerator qrCodeImageGenerator = new QrCodeImageGenerator(this);
		Bitmap qrCodeImageBitmap = qrCodeImageGenerator.generateCode(qRCodeDataModel.toString(), false);
		
		qrCodeImage.setImageBitmap(qrCodeImageBitmap);
	}

	@Override
	public void onBackPressed() {
		setResult(PREVIEW_ACTIVITY_CANCELED);
		finish();
	}
}
