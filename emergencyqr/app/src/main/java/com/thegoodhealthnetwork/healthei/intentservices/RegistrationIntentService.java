package com.thegoodhealthnetwork.healthei.intentservices;

import org.json.JSONException;
import org.json.JSONObject;

import com.thegoodhealthnetwork.healthei.R;
import com.thegoodhealthnetwork.healthei.model.SubscribeInformationModel;
import com.thegoodhealthnetwork.healthei.services.JsonPostService;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

public class RegistrationIntentService extends IntentService {
	public final static int SUCCESS = 1;
	public final static int ERROR = 2;
	private SubscribeInformationModel subscribeInformationModel;
	private Messenger messenger;

	public RegistrationIntentService() {
		super("RegistrationIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {

		Bundle extras = intent.getExtras();
		if (extras != null) {
			messenger = (Messenger) extras.get("MESSENGER");
			subscribeInformationModel = (SubscribeInformationModel) extras
					.get("subscribeInformationModel");
		}

		Message msg = Message.obtain();

		try {

			String response = JsonPostService.makeRequest(getApplicationContext().getResources().getString(R.string.pathToRegisterService),
					getMapFromModel(subscribeInformationModel));
			JSONObject jsonObject = new JSONObject(response);

			int errorExist= 0;

			if(jsonObject.has("error")) {
				errorExist = jsonObject.getInt("error");
			}

			if(errorExist==0) {
				msg.arg1 = SUCCESS;
				msg.obj = jsonObject.get("app_token");
			}
			else {

				msg.arg1 = ERROR;
				msg.obj="Unknow error";
				JSONObject error = jsonObject.getJSONObject("error_list");

				if(error!=null) {
					msg.arg2 = error.getInt("code");
					msg.obj = error.getString("message");
				}
			}
		} catch (Exception e) {
			msg.arg1 = 2;
			msg.obj = e.getMessage();
			e.printStackTrace();
		}

		try {
			messenger.send(msg);
		} catch (android.os.RemoteException e1) {
			Log.w(getClass().getName(), "Exception sending message", e1);
		}

	}

	private JSONObject getMapFromModel(SubscribeInformationModel model) throws JSONException {
		JSONObject obj = new JSONObject();

		obj.put("email", model.getEmail());
		obj.put("firstName", model.getFirstName());
		obj.put("lastName", model.getLastName());
		obj.put("zipCode", model.getZipCode());
		obj.put("country", model.getCountry());
		obj.put("phone", model.getPhone());
		obj.put("phoneType", "android");

		return obj;

	}

}
