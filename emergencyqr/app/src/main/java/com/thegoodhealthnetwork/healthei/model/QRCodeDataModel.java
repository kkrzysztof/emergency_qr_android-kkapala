package com.thegoodhealthnetwork.healthei.model;

import java.io.Serializable;

public class QRCodeDataModel implements Serializable {
	
	private static final long serialVersionUID = -9123649478197153149L;
	
	private static final String PHONE_OWNER_LABEL = "Phone owner";
	private static final String NAME_LABEL = "Name";
	private static final String EMAIL_LABEL = "E-mail";
	private static final String EMERGENCY_CONTAC_LABEL = "Emergency contact";
	private static final String PHONE_LABEL = "Phone";
	private static final String IS_HEALTH_PROXY_LABEL = "Health care proxy";
	private static final String HEALTH_INFORMATION_LABEL = "Health information";
	private static final String MEDICATIONS_LABEL = "Medications";
	private static final String CONDITION_LABEL = "Condition";
	private static final String ALLERGIES_LABEL = "Allergies";
	private static final String OTHER_LABEL = "Notes";
	private static final String RELEATIONSHIP_LABEL = "Relationship";
	
	private static final String APPLICATION_NAME_LABEL = "HealthEI";
	private static final String APPLICATION_VERSION_LABEL = "v1.0";
	
	
	private String phonneOwnerName;
	private String phoneOwnerEmail;
	private String emergencyPersonName;
	private String emergencyPersonPhone;
	private String emergencyPersonEmail;
	private String emergencyPersonRelationShip;
	private boolean isHealthProxy;
	private String medications;
	private String condition;
	private String allergies;
	private String other;
	
	public String getPhonneOwnerName() {
		return phonneOwnerName;
	}
	
	public void setPhonneOwnerName(String phonneOwnerName) {
		this.phonneOwnerName = phonneOwnerName;
	}
	
	public String getPhoneOwnerEmail() {
		return phoneOwnerEmail;
	}
	
	public void setPhoneOwnerEmail(String phoneOwnerEmail) {
		this.phoneOwnerEmail = phoneOwnerEmail;
	}
	
	public String getEmergencyPersonName() {
		return emergencyPersonName;
	}
	
	public void setEmergencyPersonName(String emergencyPersonName) {
		this.emergencyPersonName = emergencyPersonName;
	}
	
	public String getEmergencyPersonPhone() {
		return emergencyPersonPhone;
	}
	
	public void setEmergencyPersonPhone(String emergencyPersonPhone) {
		this.emergencyPersonPhone = emergencyPersonPhone;
	}
	
	public String getEmergencyPersonEmail() {
		return emergencyPersonEmail;
	}
	
	public void setEmergencyPersonEmail(String emergencyPersonEmail) {
		this.emergencyPersonEmail = emergencyPersonEmail;
	}
	
	public boolean isHealthProxy() {
		return isHealthProxy;
	}

	public void setHealthProxy(boolean isHealthProxy) {
		this.isHealthProxy = isHealthProxy;
	}

	public String getMedications() {
		return medications;
	}

	public void setMedications(String medications) {
		this.medications = medications;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getAllergies() {
		return allergies;
	}

	public void setAllergies(String allergies) {
		this.allergies = allergies;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public String getEmergencyPersonRelationShip() {
		return emergencyPersonRelationShip;
	}

	public void setEmergencyPersonRelationShip(String emergencyPersonRelationShip) {
		this.emergencyPersonRelationShip = emergencyPersonRelationShip;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append(PHONE_OWNER_LABEL);
		sb.append("\r\n");
		
		sb.append(NAME_LABEL);
		sb.append(": ").append(phonneOwnerName);
		sb.append("\r\n");
		
		sb.append(EMAIL_LABEL);
		sb.append(": ").append(phoneOwnerEmail);
		sb.append("\r\n");
		sb.append("\r\n");
		
		sb.append(EMERGENCY_CONTAC_LABEL);
		sb.append("\r\n");
		
		sb.append(NAME_LABEL);
		sb.append(": ").append(emergencyPersonName);
		sb.append("\r\n");
		
		sb.append(PHONE_LABEL);
		sb.append(": ").append(emergencyPersonPhone);
		sb.append("\r\n");
		
		sb.append(EMAIL_LABEL);
		sb.append(": ").append(emergencyPersonEmail);
		sb.append("\r\n");

		sb.append(RELEATIONSHIP_LABEL);
		sb.append(": ").append(emergencyPersonRelationShip);
		sb.append("\r\n");
		
		sb.append(IS_HEALTH_PROXY_LABEL);
		sb.append(": ");
		
		if(isHealthProxy) {
			sb.append("YES");
		}
		else {
			sb.append("NO");
		}

		sb.append("\r\n");
		sb.append("\r\n");
		
		sb.append(HEALTH_INFORMATION_LABEL);
		sb.append("\r\n");
		
		sb.append(MEDICATIONS_LABEL);
		sb.append(": ").append(medications);
		sb.append("\r\n");
		
		sb.append(CONDITION_LABEL);
		sb.append(": ").append(condition);
		sb.append("\r\n");
		
		sb.append(ALLERGIES_LABEL);
		sb.append(": ").append(allergies);
		sb.append("\r\n");
		
		sb.append(OTHER_LABEL);
		sb.append(": ").append(other);
		sb.append("\r\n");
		sb.append("\r\n");
		
		sb.append(APPLICATION_NAME_LABEL);
		sb.append(": "+APPLICATION_VERSION_LABEL);
		
		return sb.toString();
	}
	
	public static QRCodeDataModel isQRCodeDataMOdel(String value) {
		String[] lines = value.split("\r\n");
		
		if(lines.length!=18) {
			return null;
		}
		
		QRCodeDataModel qrCodeDataModel = new QRCodeDataModel();
		
		if(!lines[0].startsWith(PHONE_OWNER_LABEL)) {
			return null;
		}
		
		if(!lines[1].startsWith(NAME_LABEL)) {
			return null;
		}
		else {
			qrCodeDataModel.setPhonneOwnerName(getValueFromLine(lines[1]));
		}
		
		if(!lines[2].startsWith(EMAIL_LABEL)) {
			return null;
		}
		else {
			qrCodeDataModel.setPhoneOwnerEmail(getValueFromLine(lines[2]));
		}
		
		if(!lines[3].isEmpty()) {
			return null;
		}
		
		if(!lines[4].startsWith(EMERGENCY_CONTAC_LABEL)) {
			return null;
		}
		
		if(!lines[5].startsWith(NAME_LABEL)) {
			return null;
		}
		else {
			qrCodeDataModel.setEmergencyPersonName(getValueFromLine(lines[5]));
		}
		
		if(!lines[6].startsWith(PHONE_LABEL)) {
			return null;
		}
		else {
			qrCodeDataModel.setEmergencyPersonPhone(getValueFromLine(lines[6]));
		}
		
		if(!lines[7].startsWith(EMAIL_LABEL)) {
			return null;
		}
		else {
			qrCodeDataModel.setEmergencyPersonEmail(getValueFromLine(lines[7]));
		}

		if(!lines[8].startsWith(RELEATIONSHIP_LABEL)) {
			return null;
		}
		else {
			qrCodeDataModel.setEmergencyPersonRelationShip(getValueFromLine(lines[8]));
		}
		
		if(!lines[9].startsWith(IS_HEALTH_PROXY_LABEL)) {
			return null;
		}
		else {
			if(getValueFromLine(lines[9]).trim().toUpperCase().equals("YES")) {
				qrCodeDataModel.setHealthProxy(true);
			}
			else {
				qrCodeDataModel.setHealthProxy(false);
			}
		}
		
		if(!lines[10].isEmpty()) {
			return null;
		}
		
		if(!lines[11].startsWith(HEALTH_INFORMATION_LABEL)) {
			return null;
		}
		
		if(!lines[12].startsWith(MEDICATIONS_LABEL)) {
			return null;
		}
		else {
			qrCodeDataModel.setMedications(getValueFromLine(lines[12]));
		}
		
		if(!lines[13].startsWith(CONDITION_LABEL)) {
			return null;
		}
		else {
			qrCodeDataModel.setCondition(getValueFromLine(lines[13]));
		}
		
		if(!lines[14].startsWith(ALLERGIES_LABEL)) {
			return null;
		}
		else {
			qrCodeDataModel.setAllergies(getValueFromLine(lines[14]));
		}
		
		if(!lines[15].startsWith(OTHER_LABEL)) {
			return null;
		}
		else {
			qrCodeDataModel.setOther(getValueFromLine(lines[15]));
		}
		
		if(!lines[16].isEmpty()) {
			return null;
		}
		
		if(!lines[17].startsWith(APPLICATION_NAME_LABEL)) {
			return null;
		}
		
		return qrCodeDataModel;
	}
	
	private static String getValueFromLine(String line) {
		String[] parts = line.split(":");
		
		if(parts.length==2) {
			return parts[1];
		}
		else {
			return "";
		}
	}
	
}
