package com.thegoodhealthnetwork.healthei.activity;

import com.bugsnag.android.Bugsnag;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import io.fabric.sdk.android.Fabric;
import java.util.Date;
import com.thegoodhealthnetwork.healthei.R;
import com.thegoodhealthnetwork.healthei.dao.AppDataDao;
import com.thegoodhealthnetwork.healthei.dao.StatisticsDao;
import com.thegoodhealthnetwork.healthei.dao.SubscribeInformationDao;
import com.thegoodhealthnetwork.healthei.dialogs.InfoDialog;
import com.thegoodhealthnetwork.healthei.global.Global;
import com.thegoodhealthnetwork.healthei.global.Trial;
import com.thegoodhealthnetwork.healthei.intentservices.RegistrationIntentService;
import com.thegoodhealthnetwork.healthei.intentservices.StatisticIntentService;
import com.thegoodhealthnetwork.healthei.model.AppDataModel;
import com.thegoodhealthnetwork.healthei.model.SubscribeInformationModel;
import com.thegoodhealthnetwork.healthei.satatistic.StatisticManager;
import com.thegoodhealthnetwork.healthei.satatistic.Statistics;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;

import com.facebook.FacebookSdk;

public class SplashScreenActivity extends Activity {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "1L1dNEi8CdNTjV7uOsD6RxSdk";
    private static final String TWITTER_SECRET = "QzG2sZVqxcnT7GxEIPctmfRxkTJfu4rs7OF3gZ2B7Yxytq0EVY";

	private String appId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bugsnag.init(getApplicationContext());

		TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
		Fabric.with(getApplicationContext(), new Twitter(authConfig));

		FacebookSdk.sdkInitialize(getApplicationContext());

		setContentView(R.layout.activity_splashscreen);

		Global.setApplicationFilePaht(Environment
				.getExternalStoragePublicDirectory("goodhealt")
				.getAbsolutePath());
		Global.setIntegrnalApplicationFilePaht(getApplicationContext()
				.getFilesDir().getAbsolutePath());


		sendStatistics();

		StatisticManager
				.increaseAppStarted(new Date(System.currentTimeMillis()));

		startApp();
		
	}

	private void startApp() {
		Thread logoTimer = new Thread() {
			public void run() {
				try {
					boolean subscribed = checkSubscribeInformation();
					
					int logoTimer = 0;
					while (logoTimer < 2000) {
						sleep(100);
						logoTimer = logoTimer + 100;
					}

					if (!subscribed) {
						startActivity(new Intent(
								"com.thegoodhealthnetwork.healthei.activity.InfoActivity"));
					} else {
						startActivity(new Intent(
								"com.thegoodhealthnetwork.healthei.activity.MainActivity"));
					}
				}

				catch (InterruptedException e) {

					e.printStackTrace();
				}

				finally {
					finish();
				}
			}
		};

		logoTimer.start();
	}

	private boolean checkSubscribeInformation() {
		SubscribeInformationDao subscribeInformationDao = new SubscribeInformationDao();

		try {
			SubscribeInformationModel model = subscribeInformationDao.load();

			if (model == null) {
				return false;
			} else if (model.getAppToken() == null) {
				sendSubscriptionInformation(model);
			} else {
				appId = model.getAppToken();
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	private void sendStatistics() {

		if (appId != null) {
			StatisticsDao statisticsDao = new StatisticsDao();

			Statistics statistics = null;
			try {
				statistics = statisticsDao.load();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Intent intent = new Intent(getApplicationContext(), StatisticIntentService.class);

			Messenger messenger = new Messenger(
					new SendStatisticsResponseHandler());

			intent.putExtra("MESSENGER", messenger);
			intent.putExtra("statisticModel", statistics);
			intent.putExtra("appId", appId);

			startService(intent);
		}
	}

	private void sendSubscriptionInformation(
			SubscribeInformationModel subscribeInformationModel) {
		Intent intent = new Intent(getApplicationContext(), RegistrationIntentService.class);

		HandlerThread thread = new HandlerThread("Thread name", android.os.Process.THREAD_PRIORITY_BACKGROUND);
		thread.start();
		Looper serviceLooper = thread.getLooper();

		Messenger messenger = new Messenger(
				new SendRegistrationResponseHandler(serviceLooper));

		intent.putExtra("MESSENGER", messenger);
		intent.putExtra("subscribeInformationModel", subscribeInformationModel);

		startService(intent);

	}

	private class SendRegistrationResponseHandler extends Handler {

		public SendRegistrationResponseHandler(Looper l) {
			super(l);
		}

		public void handleMessage(Message message) {
			if (message.arg1 == RegistrationIntentService.SUCCESS) {
				String appToken = (String) message.obj;
				if (appToken != null && !appToken.isEmpty()) {
					updateAppToken(appToken);
					appId = appToken;
				}
			}
		}
	}

	private class SendStatisticsResponseHandler extends Handler {

		public SendStatisticsResponseHandler() {
			super();

		}

		public void handleMessage(Message message) {
			if (message.arg1 == RegistrationIntentService.SUCCESS) {
				StatisticManager.removeOlder();
			}
		}
	}

	private void updateAppToken(String appToken) {
		SubscribeInformationDao subscribeInformationDao = new SubscribeInformationDao();

		try {
			SubscribeInformationModel model = subscribeInformationDao.load();
			model.setAppToken(appToken);

			subscribeInformationDao.save(model);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private AppDataModel getAppData() throws Exception {
		AppDataDao appDataDao = new AppDataDao();

		AppDataModel appDataModel = appDataDao.load();

		if (appDataModel == null) {
			appDataModel = new AppDataModel();
			appDataModel.setCreateMessage1NuberOfShow(0);
			appDataModel.setCreateMessage2NuberOfShow(0);
			appDataModel.setDontShowCreateMessage1(false);
			appDataModel.setDontShowCreateMessage2(false);
			appDataModel.setActiwationDate(System.currentTimeMillis());
			appDataDao.save(appDataModel);
		}

		return appDataModel;
	}

	private void checkTrial(AppDataModel appDataModel) {
		int nuberOfTrialDays = Trial.restOfDays(appDataModel
				.getActiwationDate());

		InfoDialog infoDialog = new InfoDialog(this);
		if (nuberOfTrialDays == 0) {
			infoDialog.setInfoText(getTrialEndMessage());
			infoDialog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					System.exit(0);

				}
			});
		} else {
			infoDialog.setInfoText(getTrialMessage(nuberOfTrialDays));
			infoDialog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					startApp();
				}
			});
		}
		infoDialog.show();
	}

	private String getTrialMessage(int numberOfDays) {
		return "This is trial version of application. " + numberOfDays
				+ " days left to finish.";
	}

	private String getTrialEndMessage() {
		return "This is trial version of application. Time is over. Please buy full version.";
	}

}
