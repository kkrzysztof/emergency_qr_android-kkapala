package com.thegoodhealthnetwork.healthei.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import com.facebook.appevents.AppEventsLogger;
import com.thegoodhealthnetwork.healthei.R;
import com.thegoodhealthnetwork.healthei.model.QRCodeDataModel;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//Bugsnag.notify(new RuntimeException("Non-fatal"));

		setContentView(R.layout.activity_main);

		ImageButton qrCodeEncoderButton = (ImageButton) findViewById(R.id.qrCodeEncoderButton);
		qrCodeEncoderButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(
						"com.thegoodhealthnetwork.healthei.activity.LockScreenActivity");
				startActivity(intent);
			}
		});

		ImageButton qrCodeDecoderButton = (ImageButton) findViewById(R.id.qrCodeDecoderButton);
		qrCodeDecoderButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(
						"com.google.zxing.client.android.SCAN");
				intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
				startActivityForResult(intent, 0);
			}
		});

		ImageButton qrShareButton = (ImageButton) findViewById(R.id.qrShareButton);
		qrShareButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent("com.thegoodhealthnetwork.healthei.activity.QrShareActivity");
				startActivityForResult(intent,0);
			}
		});


	}


	@Override
	protected void onResume() {
		super.onResume();

		// Logs 'install' and 'app activate' App Events.
		AppEventsLogger.activateApp(this);
	}

	@Override
	protected void onPause() {
		super.onPause();

		// Logs 'app deactivate' App Event.
		AppEventsLogger.deactivateApp(this);
	}
	

	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == 0) {
			if (resultCode == RESULT_OK) {

				String contents = intent.getStringExtra("SCAN_RESULT");
				String format = intent.getStringExtra("SCAN_RESULT_FORMAT");

				QRCodeDataModel qRCodeDataModel = QRCodeDataModel
						.isQRCodeDataMOdel(contents);

				if (qRCodeDataModel != null) {
					Intent qrCodeDecodedActivityIntenet = new Intent(
							"com.thegoodhealthnetwork.healthei.activity.QrDecodedActivity");
					qrCodeDecodedActivityIntenet.putExtra("qrcodedata", qRCodeDataModel);
					startActivity(qrCodeDecodedActivityIntenet);
				}
				else {
					Intent unknowQrCodeDecodedActivityIntenet = new Intent(
							"com.thegoodhealthnetwork.healthei.activity.QrDecodedUnknowActivity");
					unknowQrCodeDecodedActivityIntenet.putExtra("decodetqrcode", contents);
					startActivity(unknowQrCodeDecodedActivityIntenet);
				}

			} else if (resultCode == RESULT_CANCELED) {
				Log.i("App", "Scan unsuccessful");
			}
		}
	}
}
