package com.thegoodhealthnetwork.healthei.dialogs;

import com.thegoodhealthnetwork.healthei.R;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.FrameLayout;

public class ActionsDialog extends Dialog {
	private ActionsDialogListener actionsDialogListener;
	
	 public interface ActionsDialogListener {
		    public void onPreviewClick();
	        public void onSendClick() throws Exception;
	        public void onSaveClick() throws Exception;
	        public void onPrintClick();
	        public void onShareClick();
		 	public void onComposeClick();
	   
	}
	
	public ActionsDialog(Context context) {
		super(context);

		setCanceledOnTouchOutside(false);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.actions_view);
	
		getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

		FrameLayout previewButton = (FrameLayout)findViewById(R.id.previewButton);
		previewButton.setOnClickListener(new PreviewButtonCLickListener());

		FrameLayout composeLockscreenButton = (FrameLayout)findViewById(R.id.composeLockscreenButton);
		composeLockscreenButton.setOnClickListener(new ComposeLockscreenButtonCLickListener());
		
		FrameLayout sendButton = (FrameLayout)findViewById(R.id.sendButton);
		sendButton.setOnClickListener(new SendButtonCLickListener());
		
		FrameLayout saveButton = (FrameLayout)findViewById(R.id.saveButton);
		saveButton.setOnClickListener(new SaveButtonCLickListener());
		
		FrameLayout shareButton = (FrameLayout)findViewById(R.id.shareButton);
		shareButton.setOnClickListener(new ShereButtonCLickListener());
		
		FrameLayout cancelButton = (FrameLayout)findViewById(R.id.cancelButton);
		cancelButton.setOnClickListener(new CancelButtonCLickListener());
	}
	
	
	private class SendButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			if(actionsDialogListener!=null) {
				try {
					actionsDialogListener.onSendClick();
				} catch (Exception e) {
					e.printStackTrace();
				}
				ActionsDialog.this.cancel();
			}
		}
	}
	
	private class SaveButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			if(actionsDialogListener!=null) {
				try {
					actionsDialogListener.onSaveClick();
				} catch (Exception e) {
					e.printStackTrace();
				}
				ActionsDialog.this.cancel();
			}
		}
	}
	
	private class PrintButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			if(actionsDialogListener!=null) {
				actionsDialogListener.onPrintClick();
				ActionsDialog.this.cancel();
			}
		}
	}
	
	private class ShereButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			if(actionsDialogListener!=null) {
				actionsDialogListener.onShareClick();
				ActionsDialog.this.cancel();
			}
		}
	}
	
	
	private class PreviewButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			if(actionsDialogListener!=null) {
				actionsDialogListener.onPreviewClick();
				ActionsDialog.this.cancel();
			}
		}
	}


	private class ComposeLockscreenButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			if(actionsDialogListener!=null) {
				actionsDialogListener.onComposeClick();
				ActionsDialog.this.cancel();
			}
		}
	}


	public void setActionsDialogListener(ActionsDialogListener actionsDialogListener) {
		this.actionsDialogListener = actionsDialogListener;
	}
	
	private class CancelButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			ActionsDialog.this.cancel();
		}
	}
	
}
