package com.thegoodhealthnetwork.healthei.global;

public class Trial {
	private static final int numberOfTrialDays = 30;
	
	private static final Long milisecondsInDay = (long) (1000*60*60*24);
	
	
	public static int restOfDays(Long startTime) {
		long endTime = startTime+(numberOfTrialDays*milisecondsInDay);
		
		long currentTime = System.currentTimeMillis();
		
		if(currentTime>=endTime) {
			return 0;
		}
		
		return (int) (((endTime-currentTime)/milisecondsInDay)+1);
	}

}
