package com.thegoodhealthnetwork.healthei.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import com.thegoodhealthnetwork.healthei.global.Global;
import com.thegoodhealthnetwork.healthei.model.SettingsModel;
import android.util.Log;

public class SettingsDataDao {
	private File settingsDataFile;

	private static final String TAG = "SettingsDataDao";

	public SettingsDataDao() {

		String qrCodeDataFolder = "/qrcodedata";

		File qrCodeDataFolderFile = new File(
				Global.getIntegrnalApplicationFilePaht() + qrCodeDataFolder);

		if (!qrCodeDataFolderFile.exists()) {
			qrCodeDataFolderFile.mkdir();
		}

		settingsDataFile = new File(qrCodeDataFolderFile, "settings.dat");
	}

	public void save(SettingsModel settingsModel, boolean checkValues)
			throws Exception {
		
		if (checkValues) {
			if (settingsModel.getPassword() == null
					|| settingsModel.getPassword().isEmpty()) {
				throw new Exception("Password field can't be empty");
			}

			if (settingsModel.getRepeatePassword() == null
					|| !settingsModel.getPassword().equals(
							settingsModel.getRepeatePassword())) {
				throw new Exception("Values from password fields are not equal");
			}
		}

		try {
			FileOutputStream fos = new FileOutputStream(settingsDataFile);

			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(settingsModel);
			oos.close();
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
			Log.e(TAG,
					"Can't save qrcode data to file "
							+ settingsDataFile.getAbsolutePath());
			throw new Exception("Can't save data");
		}
	}

	public SettingsModel load() throws Exception {
		SettingsModel settingsModel = new SettingsModel();

		if (!settingsDataFile.exists()) {
			return null;
		}

		try {
			FileInputStream fis = new FileInputStream(settingsDataFile);

			ObjectInputStream ois = new ObjectInputStream(fis);
			settingsModel = (SettingsModel) ois.readObject();
			ois.close();

			return settingsModel;
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
			Log.e(TAG,
					"Can't read qrcode data from file "
							+ settingsDataFile.getAbsolutePath());
			throw new Exception("Can't read settings data");
		}
	}
}