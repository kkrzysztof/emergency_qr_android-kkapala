package com.thegoodhealthnetwork.healthei.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import com.thegoodhealthnetwork.healthei.global.Global;
import com.thegoodhealthnetwork.healthei.satatistic.Statistics;
import android.util.Log;

public class StatisticsDao {
	private File statisticsDataFile;
	
	private static final String TAG = "SettingsDataDao";

	public StatisticsDao() {

		String qrCodeDataFolder = "/qrcodedata";

		File qrCodeDataFolderFile = new File(
				Global.getIntegrnalApplicationFilePaht() + qrCodeDataFolder);

		if (!qrCodeDataFolderFile.exists()) {
			qrCodeDataFolderFile.mkdir();
		}

		statisticsDataFile = new File(qrCodeDataFolderFile, "statistics.dat");
	}
	
	public void save(Statistics statistics) throws Exception {
		try {
			FileOutputStream fos = new FileOutputStream(statisticsDataFile);

			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(statistics);
			oos.close();
		} catch (IOException e) {
			Log.e(TAG,e.getMessage());
			Log.e(TAG,
					"Can't save qrcode data to file "
							+ statisticsDataFile.getAbsolutePath());
			throw new Exception("Can't save data");
		}
	}

	public Statistics load() throws Exception {
		Statistics statistics = new Statistics();

		if (!statisticsDataFile.exists()) {
			return statistics;
		}

		try {
			FileInputStream fis = new FileInputStream(statisticsDataFile);

			ObjectInputStream ois = new ObjectInputStream(fis);
			statistics = (Statistics) ois.readObject();
			ois.close();

			return statistics;
		} catch (IOException e) {
			Log.e(TAG,e.getMessage());
			Log.e(TAG,
					"Can't read data from file "
							+ statisticsDataFile.getAbsolutePath());
			throw new Exception("Can't read statistics data");
		}
	}
}
