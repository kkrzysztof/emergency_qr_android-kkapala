package com.thegoodhealthnetwork.healthei.dialogs;

import com.thegoodhealthnetwork.healthei.R;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.TextView;

public class InfoDontShowDialog extends Dialog {
	private TextView infoViewMessage;
	private TextView titletView;
	
	public InfoDontShowDialog(Context context) {
		super(context);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.info_view2);

		setCanceledOnTouchOutside(false);
		setCancelable(false);
	
		getWindow().setLayout(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
		
		FrameLayout okButton = (FrameLayout)findViewById(R.id.okButton);
		okButton.setOnClickListener(new OkButtonCLickListener());
		
		FrameLayout dontShowButton = (FrameLayout)findViewById(R.id.dontShowButton);
		dontShowButton.setOnClickListener(new DontShowButtonLickListener());
		
		
		infoViewMessage = (TextView)findViewById(R.id.infoViewMessage);
		titletView = (TextView)findViewById(R.id.titletView);
		
	}
	
	public void setInfoTitle(String text) {
		titletView.setText(text);
	}
	
	public void setInfoText(String text) {
 		infoViewMessage.setText(text);
	}
	
	public interface InfoDontShowDialogListener {
		
		public void onDontShowClick();
		public void onOkClick();
	}
	
	private class OkButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			
			if(infoDontShowDialogListener!=null) {
				infoDontShowDialogListener.onOkClick();
			}
			
			InfoDontShowDialog.this.cancel();
		}
	}
	
	private class DontShowButtonLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			
			if(infoDontShowDialogListener!=null) {
				infoDontShowDialogListener.onDontShowClick();
			}
			
			InfoDontShowDialog.this.cancel();
		}
	}
	
	
	
	private InfoDontShowDialogListener infoDontShowDialogListener;

	public InfoDontShowDialogListener getInfoDontShowDialogListener() {
		return infoDontShowDialogListener;
	}

	public void setInfoDontShowDialogListener(
			InfoDontShowDialogListener infoDontShowDialogListener) {
		this.infoDontShowDialogListener = infoDontShowDialogListener;
	}
}
