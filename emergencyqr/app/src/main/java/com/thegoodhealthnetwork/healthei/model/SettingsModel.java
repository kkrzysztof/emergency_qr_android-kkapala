package com.thegoodhealthnetwork.healthei.model;

import java.io.Serializable;

public class SettingsModel implements Serializable{
	private static final long serialVersionUID = 6895467203076134107L;
	
	private static final String TAG = "SettingsModel";
	
	private String password;
	private String repeatePassword;
	private String passwordResetQuestion;
	private String passwordResetAnswer;
	private String passwordResetEmail;
	
	private boolean passwordDisabled;
	
	public SettingsModel() {
		
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPasswordResetQuestion() {
		return passwordResetQuestion;
	}
	
	public void setPasswordResetQuestion(String passwordResetQuestion) {
		this.passwordResetQuestion = passwordResetQuestion;
	}
	
	public String getPasswordResetAnswer() {
		return passwordResetAnswer;
	}
	
	public void setPasswordResetAnswer(String passwordResetAnswer) {
		this.passwordResetAnswer = passwordResetAnswer;
	}
	
	public String getPasswordResetEmail() {
		return passwordResetEmail;
	}
	
	public void setPasswordResetEmail(String passwordResetEmail) {
		this.passwordResetEmail = passwordResetEmail;
	}
	
	public String getRepeatePassword() {
		return repeatePassword;
	}

	public void setRepeatePassword(String repeatePassword) {
		this.repeatePassword = repeatePassword;
	}

	public boolean isPasswordDisabled() {
		return passwordDisabled;
	}

	public void setPasswordDisabled(boolean passwordDisabled) {
		this.passwordDisabled = passwordDisabled;
	}

	
	
	
}
