package com.thegoodhealthnetwork.healthei.activity;

import com.thegoodhealthnetwork.healthei.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class InfoActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_info);

		Button nextButton = (Button)findViewById(R.id.nextButton);
		nextButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(
						"com.thegoodhealthnetwork.healthei.activity.TermsOfUseActivity"));
			}
		});
	}
}
