package com.thegoodhealthnetwork.healthei.satatistic;

import java.util.Date;
import com.thegoodhealthnetwork.healthei.dao.StatisticsDao;

public class StatisticManager {
	private static Statistics statistics;
	
	static {
		statistics = load();
	}

	
	private static Statistics load() {
		StatisticsDao statisticsDao = new StatisticsDao();
		try {
			return statisticsDao.load();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new Statistics();
	}
	
	
	public static void increaseOurCodeCreated(Date date) {
		DayStatisticModel dayModel = statistics.getDayStatisticModel(date);
		dayModel.setOur_code_created(dayModel.getOur_code_created()+1);
		save();
	}
	
	public static void increaseOurCodeSend(Date date) {
		DayStatisticModel dayModel = statistics.getDayStatisticModel(date);
		dayModel.setOur_code_send(dayModel.getOur_code_send()+1);
		save();
	}
	
	public static void increaseOurCodeSaved(Date date) {
		DayStatisticModel dayModel = statistics.getDayStatisticModel(date);
		dayModel.setOur_code_saved(dayModel.getOur_code_saved()+1);
		save();
	}
	
	public static void increaseCodeShared(Date date) {
		DayStatisticModel dayModel = statistics.getDayStatisticModel(date);
		dayModel.setCode_shared(dayModel.getCode_shared()+1);
		save();
	}
	
	public static void increaseOurCodeRead(Date date) {
		DayStatisticModel dayModel = statistics.getDayStatisticModel(date);
		dayModel.setOur_code_read(dayModel.getOur_code_read()+1);
		save();
	}
	
	public static void increaseCodeRead(Date date) {
		DayStatisticModel dayModel = statistics.getDayStatisticModel(date);
		dayModel.setCode_read(dayModel.getCode_read()+1);
		save();
	}
	
	public static void increaseAppStarted(Date date) {
		DayStatisticModel dayModel = statistics.getDayStatisticModel(date);
		dayModel.setApp_started(dayModel.getApp_started()+1);
		save();
	}
	
	private static void save() {
		StatisticsDao statisticsDao = new StatisticsDao();
		try {
			statisticsDao.save(statistics);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public static void increaseOurCodeShared(Date date) {
		// TODO Auto-generated method stub
		
	}
	
	public static void removeOlder() {
		statistics = new Statistics();
		save();
	}
}
