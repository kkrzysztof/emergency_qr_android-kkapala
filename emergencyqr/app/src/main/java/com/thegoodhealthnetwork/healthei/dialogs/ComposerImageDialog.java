package com.thegoodhealthnetwork.healthei.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;

import com.thegoodhealthnetwork.healthei.R;

/**
 * Created by kamil on 20.07.15.
 */
public class ComposerImageDialog extends Dialog {

    private ComposerImageDialogListener composerImageDialogListener;

    public interface ComposerImageDialogListener {
        public void onAddFromCameraClick();
        public void onAddFromPhotosClick();
        public void onCancelClick();
        public void onDeleteImageClick();
    }

    public ComposerImageDialog(Context context,boolean showDelete) {
        super(context);

        setCanceledOnTouchOutside(false);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.composer_image_actions_view);

        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        FrameLayout addFromCamerButton = (FrameLayout)findViewById(R.id.addFromCamerButton);
        addFromCamerButton.setOnClickListener(new AddFromCamerButtonCLickListener());

        FrameLayout addFromPhotosButton = (FrameLayout)findViewById(R.id.addFromPhotosButton);
        addFromPhotosButton.setOnClickListener(new AddFromPhotosButtonCLickListener());

        FrameLayout deleteImageButton = (FrameLayout) findViewById(R.id.deleteImageButton);
        deleteImageButton.setOnClickListener(new DeleteImageCLickListener());

        if(!showDelete) {
            deleteImageButton.setVisibility(View.GONE);
        }

        FrameLayout cancelButton = (FrameLayout)findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new CancelButtonCLickListener());
    }

    public ComposerImageDialogListener getComposerImageDialogListener() {
        return composerImageDialogListener;
    }

    public void setComposerImageDialogListener(ComposerImageDialogListener composerImageDialogListener) {
        this.composerImageDialogListener = composerImageDialogListener;
    }

    private class AddFromCamerButtonCLickListener implements android.view.View.OnClickListener {

        @Override
        public void onClick(View v) {
            if(composerImageDialogListener!=null) {
                composerImageDialogListener.onAddFromCameraClick();
                ComposerImageDialog.this.cancel();
            }
        }
    }

    private class AddFromPhotosButtonCLickListener implements android.view.View.OnClickListener {

        @Override
        public void onClick(View v) {
            if(composerImageDialogListener!=null) {
                composerImageDialogListener.onAddFromPhotosClick();
                ComposerImageDialog.this.cancel();
            }
        }
    }

    private class DeleteImageCLickListener implements android.view.View.OnClickListener {

        @Override
        public void onClick(View v) {
            if(composerImageDialogListener!=null) {
                composerImageDialogListener.onDeleteImageClick();
                ComposerImageDialog.this.cancel();
            }
        }
    }

    private class CancelButtonCLickListener implements android.view.View.OnClickListener {

        @Override
        public void onClick(View v) {
            ComposerImageDialog.this.cancel();
        }
    }
}
