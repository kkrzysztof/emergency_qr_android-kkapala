package com.thegoodhealthnetwork.healthei.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.thegoodhealthnetwork.healthei.R;
import com.thegoodhealthnetwork.healthei.dialogs.YesNoQuestionDialog;
import com.thegoodhealthnetwork.healthei.dialogs.YesNoQuestionDialog.YesNoQuestionDialogListener;

public class TermsOfUseActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_termsofuse);

		Button acceptButton = (Button)findViewById(R.id.acceptButton);
		acceptButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				confirmAccept();
			}
		});
	}
	
	private void confirmAccept() {
		YesNoQuestionDialog yesNoQuestionDialog = new YesNoQuestionDialog(this,"Notification","Please confirm your acceptance of our Terms of Use");
		yesNoQuestionDialog.setYesNoQuestionDialogListener(new YesNoQuestionDialogListener() {
			
			@Override
			public void onYesClick() {
				startActivity(new Intent(
						"com.thegoodhealthnetwork.healthei.activity.RegistrationActivity"));
			}
			
			@Override
			public void onNoClick() {
				// TODO Auto-generated method stub
				
			}
		});
		yesNoQuestionDialog.show();
	}
}
