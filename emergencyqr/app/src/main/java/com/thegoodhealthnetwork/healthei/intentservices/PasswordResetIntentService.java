package com.thegoodhealthnetwork.healthei.intentservices;

import org.json.JSONException;
import org.json.JSONObject;

import com.thegoodhealthnetwork.healthei.R;
import com.thegoodhealthnetwork.healthei.services.JsonPostService;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

public class PasswordResetIntentService extends IntentService {
	public final static int SUCCESS = 1;
	private Messenger messenger;

	public PasswordResetIntentService() {
		super("PasswordResetIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		String resetEmail = null;

		Bundle extras = intent.getExtras();
		if (extras != null) {
			messenger = (Messenger) extras.get("MESSENGER");
			resetEmail = extras.getString("resetEmail");
		}

		Message msg = Message.obtain();

		try {

			String response = JsonPostService.makeRequest(getApplicationContext().getResources().getString(R.string.pathToRecoveryService),
					getJSON(resetEmail));
			JSONObject jsonObject = new JSONObject(response);
			String resetCode = jsonObject.getString("val");
			
			msg.arg1 = SUCCESS;
			msg.obj = resetCode;
		} catch (Exception e) {
			msg.arg1 = 2;
			e.printStackTrace();
		}

		try {
			messenger.send(msg);
		} catch (android.os.RemoteException e1) {
			Log.w(getClass().getName(), "Exception sending message", e1);
		}

	}

	private JSONObject getJSON(String resetEmail) throws JSONException {
		JSONObject obj = new JSONObject();

		obj.put("email", resetEmail);
		
		return obj;
	}
	
	
}
