package com.thegoodhealthnetwork.healthei.dialogs;

import com.thegoodhealthnetwork.healthei.R;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

public class EditBoxDialog extends Dialog {
	private EditText editText;
	
	public interface EditBoxDialogListener {
		
		public void onOK(String value);
	}
		
	public EditBoxDialog(Context context,String title,String value) {
		super(context);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.editbox_view);

		setCanceledOnTouchOutside(false);
	
		getWindow().setLayout(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
		
		TextView titletView = (TextView)findViewById(R.id.titletView);
		titletView.setText(title);
		
		FrameLayout okButton = (FrameLayout)findViewById(R.id.okButton);
		okButton.setOnClickListener(new OKButtonCLickListener());
		
		editText = (EditText)findViewById(R.id.editText);
		editText.setText(value);
	}
	
	
	private class OKButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			if(editBoxDialogListener!=null) {
				editBoxDialogListener.onOK(editText.getText().toString());
			}
			EditBoxDialog.this.cancel();
		}
	}
	
	private EditBoxDialogListener editBoxDialogListener;

	public void setEditBoxDialogListener(EditBoxDialogListener editBoxDialogListener) {
		this.editBoxDialogListener = editBoxDialogListener;
	}
	
	
}
