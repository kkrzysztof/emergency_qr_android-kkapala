package com.thegoodhealthnetwork.healthei.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import com.thegoodhealthnetwork.healthei.R;
import com.thegoodhealthnetwork.healthei.dao.SettingsDataDao;
import com.thegoodhealthnetwork.healthei.model.SettingsModel;

public class PasswordDialog extends Dialog {
	private SettingsModel settingsModel;
	private String resetPasswordCode;
	private Context context;

	private LoginListener loginListener;

	public LoginListener getLoginListener() {
		return loginListener;
	}

	public void setLoginListener(LoginListener loginListener) {
		this.loginListener = loginListener;
	}

	public interface LoginListener {
		public void onLogin();
	}

	public interface  SettingsDialogListener {
		public void onClose();
	}

	private SettingsDialogListener settingsDialogListener;


	public PasswordDialog(Context context) {
		super(context);
		this.context = context;

		settingsModel = getSettingsModel();

		this.setCancelable(false);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.password_question_dialog);

		getWindow().setLayout(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);

		FrameLayout loginButton = (FrameLayout)findViewById(R.id.loginButton);
		loginButton.setOnClickListener(new LoginButtonClickListener());

		FrameLayout cancelButton = (FrameLayout) findViewById(R.id.cancelButton);
		cancelButton.setOnClickListener(new CancelButtonCLickListener());

	}

	private class CancelButtonCLickListener implements
			android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			PasswordDialog.this.cancel();
		}
	}

	private class LoginButtonClickListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			login();
		}
	}



	private void login() {
		EditText passwordField = (EditText)findViewById(R.id.passwordField);

		String password = passwordField.getText().toString();

		if(password==null || password.isEmpty()) {
			ErrorDialog errorDialog = new ErrorDialog(PasswordDialog.this.context, PasswordDialog.this.context.getString(R.string.errorEmptyPasswordField));
			errorDialog.show();
		}
		else {
			if(password.equals(settingsModel.getPassword())) {
				if(this.loginListener != null) {
					this.loginListener.onLogin();
				}
				PasswordDialog.this.cancel();
			}
			else {
				ErrorDialog errorDialog = new ErrorDialog( PasswordDialog.this.context,  PasswordDialog.this.context.getString(R.string.errorWrongPassword));
				errorDialog.show();
			}
		}
	}

	private SettingsModel getSettingsModel() {
		SettingsDataDao settingsDataDao = new SettingsDataDao();
		try {
			return settingsDataDao.load();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}



}
