package com.thegoodhealthnetwork.healthei.dialogs;

import com.thegoodhealthnetwork.healthei.R;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;

public class ShareQuestionDialog extends Dialog {
	private ShareQuestionListener shareQuestionListener;
	
	 public interface ShareQuestionListener {
	        public void onYesClick();
	}
	
	public void setShareQuestionListener(ShareQuestionListener shareQuestionListener) {
		this.shareQuestionListener = shareQuestionListener;
	}

	public ShareQuestionDialog(Context context) {
		super(context);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.share_question_view);
		
		this.setCancelable(false);
		
		FrameLayout yesButton = (FrameLayout)findViewById(R.id.yesButton);
		yesButton.setOnClickListener(new YesButtonCLickListener());
		
		FrameLayout noButton = (FrameLayout)findViewById(R.id.noButton);
		noButton.setOnClickListener(new NoButtonCLickListener());
	}
	
	
	private class YesButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			if(shareQuestionListener!=null) {
				shareQuestionListener.onYesClick();
				ShareQuestionDialog.this.cancel();
			}
		}
	}
	
	private class NoButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			ShareQuestionDialog.this.cancel();
		}
	}
}
