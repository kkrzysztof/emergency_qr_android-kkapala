package com.thegoodhealthnetwork.healthei.intentservices;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.thegoodhealthnetwork.healthei.R;
import com.thegoodhealthnetwork.healthei.satatistic.DayStatisticModel;
import com.thegoodhealthnetwork.healthei.satatistic.Statistics;
import com.thegoodhealthnetwork.healthei.services.JsonPostService;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

public class StatisticIntentService extends IntentService {
	public final static int SUCCESS = 1;
	private Statistics statisticModel;
	private Messenger messenger;

	public StatisticIntentService() {
		super("StatisticIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		String appId = null;

		Bundle extras = intent.getExtras();
		if (extras != null) {
			messenger = (Messenger) extras.get("MESSENGER");
			statisticModel = (Statistics) extras
					.get("statisticModel");
			
			appId = (String)extras.getString("appId");
		}

		Message msg = Message.obtain();

		try {

			String response = JsonPostService.makeRequest(getApplicationContext().getResources().getString(R.string.pathToStatisticService),
					getStatisticArray(statisticModel,appId));
			JSONObject jsonObject = new JSONObject(response);
			msg.arg1 = SUCCESS;
		} catch (Exception e) {
			msg.arg1 = 2;
			e.printStackTrace();
		}

		try {
			messenger.send(msg);
		} catch (android.os.RemoteException e1) {
			Log.w(getClass().getName(), "Exception sending message", e1);
		}
	}

	private JSONObject getStatisticArray(Statistics statisticModel,String appId) throws JSONException {
		JSONObject obj = new JSONObject();

		obj.put("appID",appId);
		
		JSONArray jsonArray = new JSONArray();
		
		List<DayStatisticModel> dayStatistics =  statisticModel.getOlder(new Date(System.currentTimeMillis()));
		
		for (DayStatisticModel dayStatistic : dayStatistics) {
			jsonArray.put(getStatistic(dayStatistic));
		}

		obj.put("statistics", jsonArray);
		return obj;
	}
	
	private JSONObject getStatistic(DayStatisticModel statisticModel) throws JSONException {
		JSONObject obj = new JSONObject();
		
		obj.put("date", getDateString(statisticModel.getDateTime()));
		obj.put("our_code_created", statisticModel.getOur_code_created());
		obj.put("our_code_send", statisticModel.getOur_code_send());
		obj.put("our_code_saved", statisticModel.getOur_code_saved());
		obj.put("code_shared", statisticModel.getCode_shared());
		obj.put("our_code_read", statisticModel.getOur_code_read());
		obj.put("code_read", statisticModel.getCode_read());
		obj.put("app_started", statisticModel.getApp_started());
		
		return obj;
	}
	
	private String getDateString(Date date) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		return simpleDateFormat.format(date);
	}

}
