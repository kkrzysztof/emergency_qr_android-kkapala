package com.thegoodhealthnetwork.healthei.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import com.thegoodhealthnetwork.healthei.R;
import com.thegoodhealthnetwork.healthei.dao.SettingsDataDao;
import com.thegoodhealthnetwork.healthei.dialogs.ErrorDialog;
import com.thegoodhealthnetwork.healthei.dialogs.PasswordEmailResetDialog;
import com.thegoodhealthnetwork.healthei.dialogs.PasswordEmailResetDialog.PasswordEmailResetDialogListener;
import com.thegoodhealthnetwork.healthei.dialogs.PasswordQuestionDialog;
import com.thegoodhealthnetwork.healthei.dialogs.PasswordQuestionDialog.PasswordQuestionListener;
import com.thegoodhealthnetwork.healthei.dialogs.PasswordResetChoseDialog;
import com.thegoodhealthnetwork.healthei.dialogs.PasswordResetChoseDialog.PasswordResetChoseListener;
import com.thegoodhealthnetwork.healthei.intentservices.PasswordResetIntentService;
import com.thegoodhealthnetwork.healthei.model.SettingsModel;

public class LockScreenActivity extends Activity {

	private SettingsModel settingsModel;
	private String resetPasswordCode;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		setContentView(R.layout.activity_lock_screen);
		
		settingsModel = getSettingsModel();
		
		FrameLayout loginButton = (FrameLayout)findViewById(R.id.loginButton);
		loginButton.setOnClickListener(new LoginButtonClickListener());
		
		FrameLayout forgotPasswordButton = (FrameLayout)findViewById(R.id.forgotPasswordButton);
		forgotPasswordButton.setOnClickListener(new ForgotPasswordButtonClickListener());
		
		if(settingsModel==null) {
			startEncoderActivity(true);
		}
		else if(settingsModel!=null && settingsModel.isPasswordDisabled()) {
			startEncoderActivity(false);
		}

	}
	
	
	private void login() {
		EditText passwordField = (EditText)findViewById(R.id.passwordField);
		
		String password = passwordField.getText().toString();
		
		if(password==null || password.isEmpty()) {
			ErrorDialog errorDialog = new ErrorDialog(LockScreenActivity.this, getString(R.string.errorEmptyPasswordField));
			errorDialog.show();
		}
		else {
			if(password.equals(settingsModel.getPassword())) {
				startEncoderActivity(false);
			}
			else {
				ErrorDialog errorDialog = new ErrorDialog(LockScreenActivity.this, getString(R.string.errorWrongPassword));
				errorDialog.show();
			}
		}
	}
	
	private void forgotPassword() {
		PasswordResetChoseDialog passwordResetChoseDialog = new PasswordResetChoseDialog(this);
		passwordResetChoseDialog.setPasswordResetChose(new PasswordResetChoseListenerImpl());
		passwordResetChoseDialog.show();
	}
	

	private class LoginButtonClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			login();
		}
	}
	
	private class ForgotPasswordButtonClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			forgotPassword();
		}
	}
	
	private SettingsModel getSettingsModel() {
		SettingsDataDao settingsDataDao = new SettingsDataDao();
		try {
			return settingsDataDao.load();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private void startEncoderActivity(boolean passwordReset) {
		Intent intent = new Intent("com.thegoodhealthnetwork.healthei.activity.QrCreaterActivity");
		intent.putExtra("passwordReset", passwordReset);
		startActivity(intent);
	}
	
	private class PasswordResetChoseListenerImpl implements PasswordResetChoseListener {

		@Override
		public void onSecurityQuestion() {
			PasswordQuestionDialog passwordQuestionDialog = new PasswordQuestionDialog(LockScreenActivity.this, settingsModel.getPasswordResetQuestion());
			passwordQuestionDialog.setPasswordQuestionListener(new PasswordQuestionListener() {
				
				@Override
				public void onAnswer(String answer) {
					if(settingsModel.getPasswordResetAnswer().equals(answer)) {
						startEncoderActivity(true);
					}
					else {
						ErrorDialog errorDialog = new ErrorDialog(LockScreenActivity.this, getString(R.string.securityWrongAnswer));
						errorDialog.show();
					}
					
				}
			});
			passwordQuestionDialog.show();
		}

		@Override
		public void onResetByEmail() {
			
			if(isNetworkAvailable()){
				resetPassword(settingsModel.getPasswordResetEmail());
			}
			else {
				showError(getString(R.string.networkConnectionError));
			}
			
		}
	}
	
	private void resetPassword(String email) {
		Intent intent = new Intent(getApplicationContext(), PasswordResetIntentService.class);

		Messenger messenger = new Messenger(new ResetPasswordResponseHandler());

		intent.putExtra("MESSENGER", messenger);
		intent.putExtra("resetEmail", email);

		startService(intent);
	}
	
	private class ResetPasswordResponseHandler extends Handler {

		public ResetPasswordResponseHandler() {
			super();

		}

		public void handleMessage(Message message) {
			if (message.arg1 == PasswordResetIntentService.SUCCESS) {
				
				resetPasswordCode = (String)message.obj;
				
				PasswordEmailResetDialog passwordEmailResetDialog = new PasswordEmailResetDialog(LockScreenActivity.this,settingsModel.getPasswordResetEmail());
				
				passwordEmailResetDialog.setPasswordEmailResetDialogListener(new PasswordEmailResetDialogListener() {
					
					@Override
					public void onOk(String codeFromEmail) {
						if(resetPasswordCode.equals(codeFromEmail)) {
							startEncoderActivity(true);
						}
						else {
							showError(getString(R.string.wrongResetCode));
						}
					}
				});
				
				
				passwordEmailResetDialog.show();
			}
			else {
				ErrorDialog errorDialog = new ErrorDialog(LockScreenActivity.this, getString(R.string.resetEmailError));
				errorDialog.show();
			}
		}
	}
	
	private boolean isNetworkAvailable() {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	
	private void showError(String error) {
		ErrorDialog errorDialog = new ErrorDialog(this,error);
		errorDialog.show();
	}
}