package com.thegoodhealthnetwork.healthei.dialogs;

import com.thegoodhealthnetwork.healthei.R;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;

public class SufficientInformationDialog extends Dialog {
	
	public SufficientInformationDialog(Context context) {
		super(context);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.sufficientinformation_view);
		
		this.setCancelable(false);
		
		FrameLayout continueButton = (FrameLayout)findViewById(R.id.continueButton);
		continueButton.setOnClickListener(new ContinueButtonCLickListener());
		
		FrameLayout createButton = (FrameLayout)findViewById(R.id.createButton);
		createButton.setOnClickListener(new CreateButtonCLickListener());
	}
	
	private class ContinueButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View arg0) {
			if(sufficientInformationDialogListener!=null) {
				sufficientInformationDialogListener.onContinueClick();
			}
			SufficientInformationDialog.this.cancel();
			
		}
	}
	
	private class CreateButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View arg0) {
			if(sufficientInformationDialogListener!=null) {
				sufficientInformationDialogListener.onCreateClick();
			}
			SufficientInformationDialog.this.cancel();
		}
	}
	
	public interface SufficientInformationDialogListener {
		public void onCreateClick();
		public void onContinueClick();
	}
	
	private SufficientInformationDialogListener sufficientInformationDialogListener;

	public SufficientInformationDialogListener getSufficientInformationDialogListener() {
		return sufficientInformationDialogListener;
	}

	public void setSufficientInformationDialogListener(
			SufficientInformationDialogListener sufficientInformationDialogListener) {
		this.sufficientInformationDialogListener = sufficientInformationDialogListener;
	}
	
	

}
