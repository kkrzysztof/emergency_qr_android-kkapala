package com.thegoodhealthnetwork.healthei.model;

import java.io.Serializable;

public class AppDataModel implements Serializable {

	private static final long serialVersionUID = 5907093446426076854L;

	private boolean dontShowCreateMessage1;
	
	private boolean dontShowCreateMessage2;

	private boolean dontShowCreateMessage3;
	
	private int createMessage1NuberOfShow;
	
	private int createMessage2NuberOfShow;

	private int createMessage3NuberOfShow;

	private Long actiwationDate;

	public AppDataModel() {
	}

	public boolean isDontShowCreateMessage1() {
		return dontShowCreateMessage1;
	}

	public void setDontShowCreateMessage1(boolean dontShowCreateMessage1) {
		this.dontShowCreateMessage1 = dontShowCreateMessage1;
	}

	public boolean isDontShowCreateMessage2() {
		return dontShowCreateMessage2;
	}

	public void setDontShowCreateMessage2(boolean dontShowCreateMessage2) {
		this.dontShowCreateMessage2 = dontShowCreateMessage2;
	}

	public int getCreateMessage1NuberOfShow() {
		return createMessage1NuberOfShow;
	}

	public void setCreateMessage1NuberOfShow(int createMessage1NuberOfShow) {
		this.createMessage1NuberOfShow = createMessage1NuberOfShow;
		
		if(createMessage1NuberOfShow>2) {
			dontShowCreateMessage1=true;
		}
		
	}

	public int getCreateMessage2NuberOfShow() {
		return createMessage2NuberOfShow;
	}

	public void setCreateMessage2NuberOfShow(int createMessage2NuberOfShow) {
		this.createMessage2NuberOfShow = createMessage2NuberOfShow;
		
		if(createMessage2NuberOfShow>2) {
			dontShowCreateMessage2=true;
		}
	}

	public Long getActiwationDate() {
		return actiwationDate;
	}

	public void setActiwationDate(Long actiwationDate) {
		this.actiwationDate = actiwationDate;
	}

	public boolean isDontShowCreateMessage3() {
		return dontShowCreateMessage3;
	}

	public void setDontShowCreateMessage3(boolean dontShowCreateMessage3) {
		this.dontShowCreateMessage3 = dontShowCreateMessage3;
	}

	public void increaseMessage3Counter() {
		this.createMessage3NuberOfShow++;
	}

	public int getCreateMessage3NuberOfShow() {
		return createMessage3NuberOfShow;
	}

	public void setCreateMessage3NuberOfShow(int createMessage3NuberOfShow) {
		this.createMessage3NuberOfShow = createMessage3NuberOfShow;
	}
}
