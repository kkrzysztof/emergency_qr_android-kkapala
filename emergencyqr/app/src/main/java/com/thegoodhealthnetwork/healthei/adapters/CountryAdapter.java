package com.thegoodhealthnetwork.healthei.adapters;

import java.util.List;

import com.thegoodhealthnetwork.healthei.R;
import com.thegoodhealthnetwork.healthei.global.Country;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;

public class CountryAdapter extends ArrayAdapter<String> {
	private Context context;
	int layoutResourceId;
	List<String> data = null;
	int selectedPosition;
	private RadioButton mCurrentlyCheckedRB;

	public CountryAdapter(Context context,
			List<String> data) {
		super(context, R.layout.country_list_row, data);
		this.context = context;
		this.layoutResourceId = R.layout.country_list_row;
		this.data = data;
		selectedPosition = 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View row = convertView;
		CountryHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new CountryHolder();
			holder.country = (RadioButton) row.findViewById(R.id.countryChose);

			row.setTag(holder);
		} else {
			holder = (CountryHolder) row.getTag();
		}

		holder.country.setText(data.get(position));

		if (selectedPosition == position) {
			holder.country.setChecked(true);
			mCurrentlyCheckedRB = holder.country;
		} else {
			holder.country.setChecked(false);
		}
		
		if(mCurrentlyCheckedRB==null && selectedPosition==0) {
			mCurrentlyCheckedRB = holder.country;
			mCurrentlyCheckedRB.setChecked(true);
		}
		
		holder.country.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (mCurrentlyCheckedRB != null) {
                	 mCurrentlyCheckedRB.setChecked(false);
                }
               
                ((RadioButton) v).setChecked(true);
                mCurrentlyCheckedRB = (RadioButton) v;
                selectedPosition = position;
            }
        });

		return row;
	}
	
	public String getSelectedCountry() {
		return data.get(selectedPosition);
	}

	static class CountryHolder {
		RadioButton country;
	}
	
	public int getSelectCountryPosition(String selectedCountry) {
		int position=0;

		String countryName = Country.getCountryName(this.context,selectedCountry);

		for (String country  : data) {
			if(country.toUpperCase().equals(countryName.toUpperCase())) {
				selectedPosition = position;
			}
			position++;
		}
		return selectedPosition;
	}
}
